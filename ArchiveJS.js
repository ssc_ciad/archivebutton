
<script type="text/javascript" language="javascript" src="/_layouts/SPP.ContentDesigner/Scripts/SP.UI.Dialog.js"></script>
<input type="hidden" name="_wpSelected" id="_wpSelected" value="MSOZoneCell_WebPartWPQ1" />
<script src="/_layouts/15/sp.runtime.js"></script>
<script src="/_layouts/15/sp.js"></script>
<script src="/sites/031640/SiteAssets/Scripts/jquery.min.js"></script>
<script src="/_layouts/15/MicrosoftAjax.js"></script>
<script language="javascript" type="text/javascript" />
 //remember to adjust script locatiob like jquery etc. (here is for 041814 workplace)   
  //get folder tree of current file -nnarray
var dataResults;
var nn

try {
    SP.UI.ModalDialog.showModalDialog(options);
}
catch (error) {
    SP.SOD.execute('sp.ui.dialog.js', 'SP.UI.ModalDialog.showModalDialog');
}

$( document ).ready(function() {
var x = $('#pageTitle > span > span')
var str = x[0].textContent
nn= str.split("  ");
nn = nn.filter(function() { return true; });
for(var x in nn){   
    nn[x]= nn[x].trim(); 
}
 nn.shift()


if (nn.length > 1) {
SP.SOD.executeFunc("callout.js", "Callout", function () {
  var itemCtx = {};
  itemCtx.Templates = {};
  itemCtx.BaseViewID = 'Callout';
  // Define the list template type
  itemCtx.ListTemplateType = 101;
  itemCtx.Templates.Footer = function (itemCtx) {
    // context, custom action function, show the ECB menu (booleann)
    return CalloutRenderFooterTemplate(itemCtx, AddCustomAction, true);
  };
  SPClientTemplates.TemplateManager.RegisterTemplateOverrides(itemCtx);
});
}

});

function AddCustomAction (renderCtx, calloutActionMenu) {      
    calloutActionMenu.addAction(new CalloutAction({
        text: 'Edit',
        onClickCallback: function () {
             var listSource = _spPageContextInfo.serverRequestPath;
            window.location = renderCtx.editFormUrl + "&ID=" + renderCtx.CurrentItem.ID + "&Source=" + listSource;
        }
    }))
  var editPropUrl = renderCtx.CurrentItem.FileRef
  calloutActionMenu.addAction (new CalloutAction ({
    text: "Move to Archive",
    tooltip: 'Move this file to archive',
    onClickCallback: function() {movedoc()}
  }));
}
//function to move files
function movedoc()  {  
      var files = [];  
      var context = SP.ClientContext.get_current();  
      var web = context.get_web(); context.load(web);  
      var targetlib = web.get_lists().getByTitle(nn[0] + ' Archive');  
      context.load(targetlib);  
      var notifyId;  
      var currentLib =  web.get_lists().getByTitle(nn[0])
      var selectedItems = SP.ListOperation.Selection.getSelectedItems(context);  
      var count = CountDictionary(selectedItems);  
   if (count == 0)  
   {  
       alert('Please choose at least  one file to move.');  
   }  
   for (var i in selectedItems)  
   {  
       var currentItem = currentLib.getItemById($("tr.s4-itm-selected").attr("iid").split(",")[1]);  
       context.load(currentItem); var fle = currentItem.get_file();  
  
       files.push(fle);  
       context.load(fle);  
   }  
      context.executeQueryAsync(  
          function (sender, args)  
          {  
              for (var i = 0; i < files.length; i++)  
              {  
                  var File = files[i];  
                  if (File != null)  
                  {  
                    var destinationlibUrl = nn[0] + " Archive"
                      var targetlibUrl = web.get_serverRelativeUrl() + '/' + nn[0] + ' Archive/' + nn[1] +'/' + File.get_name(); File.moveTo(targetlibUrl, 1); 
                      notifyId = SP.UI.Notify.addNotification('Moving file ' + File.get_serverRelativeUrl() + ' to ' + destinationlibUrl, true);  
                      context.executeQueryAsync(  
                          function (sender, args)  
                          {  
                              SP.UI.Notify.removeNotification(notifyId); SP.UI.Notify.addNotification('File moved successfully', false);  
                              __doPostBack("ctl00$ctl39$g_79754015_51ee_4c49_aa2d_1d203c946b34$ctl04","cancel");return false
                          },  
                          function (sender, args) {
                              alert(args.get_message())  
                               SP.UI.Notify.addNotification('Error moving file: ' + args.get_message(), false);  
                               SP.UI.Notify.removeNotification(notifyId);   
                      });  
                  }  
              }  
          }, function (sender, args) {  
              alert('Error occured' + args.get_message());  
   })  
     };
     
     if(typeof String.prototype.trim !== 'function') {
        String.prototype.trim = function() {
          return this.replace(/^\s+|\s+$/g, ''); 
        }
      }
</script>